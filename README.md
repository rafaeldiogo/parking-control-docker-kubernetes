# Projeto para conclusão da disciplina Devops para a Residência em TI no IMD/TST

### No projeto foi usado:
* JDK 11
* Maven
* Postman
* PgAdmin (Postgres)
* Spring Boot

### O projeto cntém as seguintes etapas:
* Compilação/ build
* Checagem de estilo com sonarcloud
* Criação e push de imagem Docker
* Execução de testes
* Deploy em cluster Kubernetes

### Detalhes
* Para exportar o site foi utilizado o comando: kubectl expose deployment parking-control --type=LoadBalancer --name=my-service
* Acesso ao site em: http://34.95.253.17:8080/
* Para rodar o projeto tem que criar o banco: parking-control-db e usar o comando docker-compose up no terminal
